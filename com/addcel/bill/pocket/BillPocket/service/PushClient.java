package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.client.PushRequest;
import com.addcel.bill.pocket.BillPocket.client.PushResponse;

public interface PushClient {
  PushResponse sendNotification(PushRequest paramPushRequest);
}


/* Location:              /home/hhernandez/Downloads/!/com/addcel/bill/pocket/BillPocket/service/PushClient.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */