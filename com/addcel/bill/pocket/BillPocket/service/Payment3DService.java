package com.addcel.bill.pocket.BillPocket.service;

import com.addcel.bill.pocket.BillPocket.bean.ResponseCheckout;
import com.addcel.bill.pocket.BillPocket.bean.TransactionRequest;

public interface Payment3DService {
  ResponseCheckout checkout(Integer paramInteger1, Integer paramInteger2, String paramString, TransactionRequest paramTransactionRequest);
}


/* Location:              /home/hhernandez/Downloads/!/com/addcel/bill/pocket/BillPocket/service/Payment3DService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */