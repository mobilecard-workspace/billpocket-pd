/*    */ package com.addcel.bill.pocket.BillPocket.antad;
/*    */ 
/*    */ import javax.xml.bind.annotation.XmlAccessType;
/*    */ import javax.xml.bind.annotation.XmlAccessorType;
/*    */ import javax.xml.bind.annotation.XmlElement;
/*    */ import javax.xml.bind.annotation.XmlRootElement;
/*    */ import javax.xml.bind.annotation.XmlType;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @XmlAccessorType(XmlAccessType.FIELD)
/*    */ @XmlType(name = "consultaResponse", propOrder = {"_return"})
/*    */ @XmlRootElement(name = "consultaResponse")
/*    */ public class ConsultaResponse
/*    */ {
/*    */   @XmlElement(required = true, name = "return")
/*    */   protected String _return;
/*    */   
/*    */   public String getReturn() {
/* 52 */     return this._return;
/*    */   }
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   public void setReturn(String value) {
/* 64 */     this._return = value;
/*    */   }
/*    */ }


/* Location:              /home/hhernandez/Downloads/!/com/addcel/bill/pocket/BillPocket/antad/ConsultaResponse.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */