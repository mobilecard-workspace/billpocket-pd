package com.addcel.bill.pocket.BillPocket.model.repository;

import com.addcel.bill.pocket.BillPocket.model.domain.Tarjetas;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface TarjetasRepository extends CrudRepository<Tarjetas, Long> {
  Optional<Tarjetas> findByIdTarjeta(Integer paramInteger);
}


/* Location:              /home/hhernandez/Downloads/!/com/addcel/bill/pocket/BillPocket/model/repository/TarjetasRepository.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */