package com.addcel.bill.pocket.BillPocket.model.repository;

import com.addcel.bill.pocket.BillPocket.model.domain.Usuario;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
  Optional<Usuario> findByIdUsuario(Long paramLong);
}


/* Location:              /home/hhernandez/Downloads/!/com/addcel/bill/pocket/BillPocket/model/repository/UsuarioRepository.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */