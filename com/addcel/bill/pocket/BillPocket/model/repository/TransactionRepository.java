package com.addcel.bill.pocket.BillPocket.model.repository;

import com.addcel.bill.pocket.BillPocket.model.domain.Transaction;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {
  Optional<Transaction> findByIdBitacora(long paramLong);
}


/* Location:              /home/hhernandez/Downloads/!/com/addcel/bill/pocket/BillPocket/model/repository/TransactionRepository.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */